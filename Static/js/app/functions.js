// highlights the currently selected button in navbar

function highlight_selected() {
    var radio_btn = document.getElementsByName("r");

    for (var i = 0; i < radio_btn.length; i++) {
        if (radio_btn[i].checked) {
            document.querySelector("label[for='" + radio_btn[i].getAttribute("id") + "']").parentElement.style.color = "black";
            document.querySelector("label[for='" + radio_btn[i].getAttribute("id") + "']").parentElement.style.background = "white";
            document.querySelector("label[for='" + radio_btn[i].getAttribute("id") + "']").parentElement.style.borderRadius = "4px";
        } else {
            document.querySelector("label[for='" + radio_btn[i].getAttribute("id") + "']").parentElement.style.removeProperty("background");
            document.querySelector("label[for='" + radio_btn[i].getAttribute("id") + "']").parentElement.style.removeProperty("color");
            document.querySelector("label[for='" + radio_btn[i].getAttribute("id") + "']").parentElement.style.removeProperty("border-radius");
        }
    }
}

function speak_status() {
    var synth = window.speechSynthesis;
    if (document.getElementById("switch").checked) {
        var utterThis = new SpeechSynthesisUtterance("Voice Module Enabled");
    } else {
        var utterThis = new SpeechSynthesisUtterance("Voice Module Disabled");
    }
    synth.speak(utterThis);
}