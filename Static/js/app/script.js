window.onload = highlight_selected();

// Initialization of Pattern

var canvasDiv = document.getElementById('about-page');
var options = {
    particleColor: '#888',
    background: 'Resource/Images/Cityscape.jpg',
    interactive: false,
    speed: 'medium',
    density: 'high'
};
var particleCanvas = new ParticleNetwork(canvasDiv, options);

// Injection of about text in about page

var about_txt = "This is a passion project from bunch of guys who believe that no person's disability should prevent them from engaging with technology , specially something as essential as the web. Backed by Open Source community this project is free to use for all <br/>❤";
var d = document.createElement("div");
var write_place = document.querySelector("#about-page div");
write_place.appendChild(d);
write_place.querySelector("div").innerHTML += about_txt;
write_place.querySelector("div").className += "about-text";