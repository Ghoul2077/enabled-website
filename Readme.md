<h1>Enabled APP</h1>
This is a web app that makes any website navigable and interactable for differently abled people while requiring minimal effort from the end of website owner.

<h1>Requirements</h1>
This web app only requires a browser to work, the following is a table of supported ones

<table>
	<tr>
		<td></td>
		<th colspan="6"><img src="Resource/Images/desktop.svg" style="margin: 0 auto;"></th>
		<th colspan="4"><img src="Resource/Images/mobile.svg"></th>
	</tr>
	<tr>
		<td><strong>Browsers</strong></td>
		<td>Chrome</td>
		<td>Firefox</td>
		<td>Internet Explorer</td>
		<td>Edge</td>
		<td>Safari</td>
		<td>Opera</td>
		<td>Chrome</td>
		<td>Firefox</td>
		<td>Opera</td>
		<td>Safari</td>
	</tr>
	<tr>
		<td><strong>Support</strong></td>
		<td>Yes</td>
		<td>Yes</td>
		<td>No</td>
		<td>Yes</td>
		<td>Yes</td>
		<td>Yes</td>
		<td>Yes</td>
		<td>Yes</td>
		<td>No</td>
		<td>Yes</td>
	</tr>
	<tr>
		<td><strong>Version</strong></td>
		<td>33+</td>
		<td>49+</td>
		<td>-</td>
		<td>-</td>
		<td>7+</td>
		<td>21+</td>
		<td>33+</td>
		<td>62+</td>
		<td>-</td>
		<td>7+</td>
	</tr>
</table>

You are free to try out this app on unsupported browser but we not gaurantee that all features will work as Intended.

<h1>Usage</h1>
To setup the app just clone the repository using the command

> git clone https://gitlab.com/Ghoul2077/enabled-website

then run the app by running index.html file in browser of your choice

<h2>Or</h2>

 Alternativly you can visit the following URL to check out the app live

> https://ghoul2077.gitlab.io/enabled-website

<h1>License</h1>

> This project is licensed under GNU General Public License v3
